using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace atomicc.Data {
    public class GuildData {
        [JsonProperty] private readonly ulong _guildId;

        [JsonIgnore] public ulong GuildId => _guildId;

        [JsonProperty("rp channels")] private readonly HashSet<ulong> _rpChannels;

        [JsonIgnore] public ImmutableHashSet<ulong> RpChannels => _rpChannels.ToImmutableHashSet();

        [JsonProperty] public readonly List<Character> Characters;

        public GuildData(ulong guildId) {
            _guildId = guildId;
            _rpChannels = new HashSet<ulong>();
            Characters = new List<Character>();
        }

        [JsonConstructor]
        public GuildData(ulong guildId, HashSet<ulong> rpChannels, List<Character> characters) {
            _rpChannels = rpChannels;
            Characters = characters;
            _guildId = guildId;
        }

        public void NewCharacter(string fullName, HashSet<string> aliases, string @group, ulong assignedUserId = 0,
            MessageRepresentation? lastRpMessage = null) {
            var nextId = Characters.Count == 0 ? 0 : Characters.Max(character => character.Id) + 1;

            var @char = new Character(nextId, fullName, aliases, group, assignedUserId, lastRpMessage);
            Characters.Add(@char);
        }

        public Character? CharacterByIdOrName(string idOrName) {
            var isNum = int.TryParse(idOrName, out var id);

            if (isNum) return Characters.Find(@char => @char.Id == id);
            return Characters.Find(
                @char => @char.FullName.Equals(idOrName, StringComparison.InvariantCultureIgnoreCase));
        }

        public void AddRpChannel(DiscordChannel channel) {
            var id = channel.Id;

            _rpChannels.Add(id);
        }

        public void RemoveRpChannel(DiscordChannel channel) {
            var id = channel.Id;
            if (!_rpChannels.Contains(id)) return;

            _rpChannels.Remove(id);
        }

        public bool IsChannelEligibleForRp(DiscordChannel channel) {
            return _rpChannels.Contains(channel.Id) ||
                   channel.Parent != null && _rpChannels.Contains(channel.Parent.Id);
        }

        public override bool Equals(object obj) {
            if (obj is GuildData dat) {
                return dat._guildId == _guildId;
            }

            return false;
        }

        public override int GetHashCode() {
            return _guildId.GetHashCode();
        }
    }
}
