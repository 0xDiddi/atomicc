using System;
using atomicc.Util;
using DSharpPlus.Entities;
using Newtonsoft.Json;

namespace atomicc.Data {
    public class MessageRepresentation {
        [JsonProperty] private readonly ulong _guildId;
        [JsonProperty] private readonly ulong _channelId;
        [JsonProperty] private readonly ulong _messageId;
        [JsonProperty] public readonly DateTime Timestamp;

        [JsonConstructor]
        public MessageRepresentation(ulong guildId, ulong channelId, ulong messageId, DateTime timestamp) {
            _guildId = guildId;
            _channelId = channelId;
            _messageId = messageId;
            Timestamp = timestamp;
        }

        public MessageRepresentation(DiscordMessage msg) {
            _guildId = msg.Channel.GuildId;
            _channelId = msg.ChannelId;
            _messageId = msg.Id;
            Timestamp = msg.Timestamp.UtcDateTime;
        }

        public string ToDirectLink() {
            return $"https://discordapp.com/channels/{_guildId}/{_channelId}/{_messageId}";
        }

        public (string ts, string delta) GetTimeDisplay() {
            var delta = DateTime.UtcNow - Timestamp;
            return (Timestamp.ToString("g"), delta.DisplayFormat());
        }

        public override bool Equals(object obj) {
            if (obj is MessageRepresentation msg) {
                return msg._guildId == _guildId &&
                       msg._channelId == _channelId &&
                       msg._messageId == _messageId;
            }

            return false;
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = _guildId.GetHashCode();
                hashCode = (hashCode * 397) ^ _channelId.GetHashCode();
                hashCode = (hashCode * 397) ^ _messageId.GetHashCode();
                return hashCode;
            }
        }
    }
}