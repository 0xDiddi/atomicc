using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace atomicc.Util {
    public class IsNotDmChannelAttribute : CheckBaseAttribute {
        public override Task<bool> CanExecute(CommandContext ctx, bool help) {
            return Task.FromResult(!ctx.Channel.IsPrivate);
        }
    }
}
