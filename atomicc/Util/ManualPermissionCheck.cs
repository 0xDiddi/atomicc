using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace atomicc.Util {
    public class ManualPermissionCheck : CheckBaseAttribute {
        public string Message { get; }
        
        public ManualPermissionCheck(string message) {
            Message = message;
        }

        public override Task<bool> CanExecute(CommandContext ctx, bool help) => Task.FromResult(false);
    }
}
