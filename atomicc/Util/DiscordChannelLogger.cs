using System;
using System.Runtime.InteropServices;
using DSharpPlus.Entities;
using Serilog.Core;
using Serilog.Events;

namespace atomicc.Util {
    public class DiscordChannelLogger : ILogEventSink {
        public DiscordChannel? Channel { get; set; }

        public void Emit(LogEvent logEvent) {
            if (Channel is null) return;

            var desc = logEvent.RenderMessage();
            if (!(logEvent.Exception is null)) {
                var exception = logEvent.Exception.ToString();
                var maxExceptionLength = 2048 - desc.Length - 7;
                if (exception.Length > maxExceptionLength) {
                    exception = exception.Substring(0, maxExceptionLength);
                }

                desc += $"\n```\n{exception}```";
            }

            var embed = new DiscordEmbedBuilder {
                Color = ColorForLevel(logEvent.Level),
                Timestamp = logEvent.Timestamp,
                Description = desc,
            }.WithFooter(logEvent.Level.ToString());

            foreach (var (key, value) in logEvent.Properties)
                embed.AddField(key, value.ToString());

            Channel.SendMessageAsync(embed: embed).RunSynchronously();
        }

        private static DiscordColor ColorForLevel(LogEventLevel lvl) =>
            lvl switch {
                LogEventLevel.Fatal => DiscordColor.Magenta,
                LogEventLevel.Error => DiscordColor.Red,
                LogEventLevel.Warning => DiscordColor.Orange,
                LogEventLevel.Information => DiscordColor.Azure,
                LogEventLevel.Debug => DiscordColor.Blurple,
                LogEventLevel.Verbose => DiscordColor.Grayple,
                _ => DiscordColor.NotQuiteBlack,
            };
    }
}
