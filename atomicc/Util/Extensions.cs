using System;
using System.Collections.Generic;
using System.Linq;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Exceptions;
using Serilog;
using Serilog.Core;
using Serilog.Core.Enrichers;

namespace atomicc.Util {
    public static class Extensions {
        public static void Deconstruct<T1, T2>(this KeyValuePair<T1, T2> tuple, out T1 key, out T2 value) {
            key = tuple.Key;
            value = tuple.Value;
        }

        public static string DisplayFormat(this TimeSpan span) {
            return $"{span.Days}d {span.Hours}h {span.Minutes}m";
        }

        public static string DetailDisplayFormat(this TimeSpan span) {
            return $"{span.Hours}h {span.Minutes}m {span.Seconds}s {span.Milliseconds}ms";
        }

        public static string DiscordDisplayFormat(this HashSet<string> set) {
            return set.Count == 0
                ? "No aliases defined."
                : set.Select(itm => $"`{itm}`").Aggregate((res, itm) => res + ", " + itm);
        }

        public static ILogger ForContext(this Logger logger, params (string key, object val)[] values) =>
            logger.ForContext(values.Select(value =>
                new PropertyEnricher(value.key, value.val, true)));

        public static void ManualPermissionCheckFailed(this CommandContext ctx, string message) {
            throw new ChecksFailedException(ctx.Command, ctx, new []{new ManualPermissionCheck(message) });
        }
    }
}
