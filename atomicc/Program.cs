﻿namespace atomicc {
    internal static class Program {
        // https://discordapp.com/oauth2/authorize?client_id=684802313960947761&scope=bot&permissions=68608
        public static void Main() {
            using var b = new Bot();
            b.RunAsync().Wait();
        }
    }
}
