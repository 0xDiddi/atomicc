using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using atomicc.Data;
using atomicc.Util;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Serilog.Core;
using Serilog.Events;

namespace atomicc.Commands {
    [Group("internal"), RequireOwner, Hidden]
    public class OwnerCommands {
        [Command("shutdown")]
        public async Task Shutdown(CommandContext ctx) {
            await ctx.RespondAsync("o7");

            await ctx.Dependencies.GetDependency<Storage>().FlushCache(true);

            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild?.Id.ToString() ?? "N/A"), ("op", "shutdown"));
            logger.Information("shutting down.");

            ctx.Dependencies.GetDependency<ManagementData>().Cts.Cancel();
        }

        [Command("set-log-level")]
        public async Task SetLogLevel(CommandContext ctx, string newLevel) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild?.Id.ToString() ?? "N/A"), ("op", "set-log-level"));
            var data = ctx.Dependencies.GetDependency<ManagementData>();

            var success = Enum.TryParse<LogEventLevel>(newLevel, true, out var level);
            if (success) {
                data.MinLogLevel.MinimumLevel = level;
                logger.Information("log level changed to {level}", level.ToString("G"));

                await ctx.RespondAsync($"Log level changed to `{level:G}`");
            } else {
                logger.Verbose("log level unchanged because {level} is not a valid log level.", newLevel);

                await ctx.RespondAsync($"Log level {newLevel} not found. Logger not adjusted.");
            }
        }

        [Command("sudo")]
        public async Task SudoCommand(CommandContext ctx, DiscordUser user, [RemainingText] string msg) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild?.Id.ToString() ?? "N/A"), ("op", "sudo"));

            logger.Information("Executing sudo command as {user} in {channel}: '{message}'",
                user.Username,
                ctx.Channel.Name,
                msg);

            await ctx.CommandsNext.SudoAsync(user, ctx.Channel, msg);
        }

        [Group("cache")]
        public class CacheCommands {
            [Command("stats")]
            public async Task Stats(CommandContext ctx) {
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild?.Id.ToString() ?? "N/A"), ("op", "cache stats"));

                var store = ctx.Dependencies.GetDependency<Storage>();
                var (total, onTimer) = store.CacheStats();
                var timeout = new TimeSpan(store.CacheTimeout * TimeSpan.TicksPerMillisecond);

                logger.Information(
                    "Cache stats retrieved: {total} in cache, {waiting} on timer. Current Timeout: {timeout}",
                    total,
                    onTimer,
                    timeout.DetailDisplayFormat());

                var embed = new DiscordEmbedBuilder();
                embed.AddField("Timeout", timeout.DetailDisplayFormat(), true);
                embed.AddField("Total", total.ToString("N0"), true);
                embed.AddField("On timeout", onTimer.ToString("N0"), true);

                await ctx.RespondAsync(embed: embed);
            }

            [Command("flush")]
            public async Task Flush(CommandContext ctx) {
                var time = await ctx.Dependencies.GetDependency<Storage>().FlushCache();

                await ctx.RespondAsync($"Cache flush done in {time.DetailDisplayFormat()}.");
            }

            [Command("timeout")]
            public async Task SetTimeout(CommandContext ctx, TimeSpan timeout) {
                ctx.Dependencies.GetDependency<Storage>().CacheTimeout = (int) timeout.TotalMilliseconds;

                await ctx.RespondAsync($"Cache timeout successfully set to {timeout.DetailDisplayFormat()}");
            }
        }

        [Group("status")]
        public class StatusCommands {
            [Command("set")]
            public async Task SetStatus(CommandContext ctx, string status, string game = null) {
                var logger = ctx.Dependencies.GetDependency<Logger>()
                    .ForContext(("guild", ctx.Guild?.Id.ToString() ?? "N/A"), ("op", "status set"));

                var stat = status switch {
                    "o" => UserStatus.Online,
                    "i" => UserStatus.Idle,
                    "dnd" => UserStatus.DoNotDisturb,
                    "inv" => UserStatus.Invisible,
                    _ => UserStatus.Online,
                };

                var discordGame = game != null ? new DiscordGame(game) : null;

                logger.Information("Upating bot status: is now {status}, playing '{game}'", stat.ToString("G"), game);

                await ctx.Client.UpdateStatusAsync(discordGame, stat);
            }
        }
    }
}
