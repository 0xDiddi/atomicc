using System.Threading.Tasks;
using atomicc.Util;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Serilog.Core;

namespace atomicc.Commands {
    [Group("channel")]
    [Description("Commands related to managing designated rp channels.")]
    [RequireUserPermissions(Permissions.ManageChannels), IsNotDmChannel]
    public class ChannelCommands {
        [Command("mark")]
        [Description("Marks a channel (group) as rp channels. These will be tracked to determine character activity.")]
        public async Task AddRpChannel(CommandContext ctx,
            [Description("The channel or channel group to mark. If unspecified, the current channel will be marked.")]
            DiscordChannel channel = null) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "channel mark"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            if (channel is null) channel = ctx.Channel;

            data.AddRpChannel(channel);
            logger.Verbose("marked channel '{channel}' in {guild}", channel.Name, ctx.Guild.Id);

            await store.CheckInGuild(data);

            var resp = channel.IsCategory
                ? $"Successfully marked all channels in {channel.Name} as rp channels."
                : $"Successfully marked {channel.Name} as an rp channel.";

            await ctx.RespondAsync(resp);
        }

        [Command("unmark")]
        [Description(
            "Removes a channel (group) from the rp channels. Channels in a marked group cannot be individually unmarked.")]
        public async Task RemoveRpChannel(CommandContext ctx,
            [Description(
                "The channel or channel group to unmark. If unspecified, the current channel will be unmarked.")]
            DiscordChannel channel = null) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "channel unmark"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.CheckOutGuild(ctx.Guild.Id);

            if (channel is null) channel = ctx.Channel;

            data.RemoveRpChannel(channel);
            logger.Verbose("unmarked channel '{channel}' in {guild}", channel.Name, ctx.Guild.Id);

            await store.CheckInGuild(data);

            var resp = channel.IsCategory
                ? $"Successfully unmarked all channels in {channel.Name} as rp channels."
                : $"Successfully unmarked {channel.Name} as an rp channel.";

            await ctx.RespondAsync(resp);
        }

        [Command("list")]
        [Description("Lists all currently marked rp channels.")]
        public async Task ListRpChannels(CommandContext ctx) {
            await ctx.TriggerTypingAsync();
            var logger = ctx.Dependencies.GetDependency<Logger>()
                .ForContext(("guild", ctx.Guild.Id), ("op", "channel list"));

            var store = ctx.Dependencies.GetDependency<Storage>();
            var data = await store.UncheckedReadonlyGuild(ctx.Guild.Id);

            if (data.RpChannels.IsEmpty) {
                await ctx.RespondAsync(
                    "No channels have been marked yet. See `atomicc:: help channel mark` for more info.");
                return;
            }

            var msg = "The following channels and categories are marked to track RP activity:\n";
            foreach (var channelId in data.RpChannels) {
                var channel = await ctx.Client.GetChannelAsync(channelId);

                if (channel.IsCategory)
                    msg += $"・All channels in `{channel.Name}`\n";
                else
                    msg += $"・{channel.Mention}\n";
            }

            await ctx.RespondAsync(msg);
        }
    }
}
