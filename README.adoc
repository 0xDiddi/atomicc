= Automatic Tracking Of Multiple In-use Canon Characters

Are you running an roleplay discord and have a hard time keeping track of canon character assignments,
especially how long it's been since a character has last been used in RP?
Then this bot is for you! It does exactly that, with no unnecessary noise.

Click link:https://discordapp.com/oauth2/authorize?client_id=684802313960947761&scope=bot&permissions=68608[this link]
to add atomicc to your discord server, and check out
link:https://gitlab.com/0xDiddi/atomicc/-/wikis/Quickstart[the quickstart guide] to get started.

== License

This program is published under the GNU GPL 3.0 License. For more details see the LICENSE file.
